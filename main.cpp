#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QFont>
#include <QDebug>

#include "config.h"
#include "uploader.h"
#include "fileholder.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setApplicationName("Cliente FTP");
    QGuiApplication::setFont(QFont("Roboto"));
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/qml/main.qml")));

    Uploader* upl = new Uploader();
    upl->run();

    return app.exec();
}
