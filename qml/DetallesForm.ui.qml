import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Item {
    id: detallesForm
    width: 400
    height: 400

    property alias nombre: lbl_nombre_detalles.text
    property alias duenio: lbl_duenio_detalles.text
    property alias fSubida: lbl_fec_sub_detalles.text
    property alias fModificacion: lbl_fec_mod_detalles.text
    property alias tamanio: lbl_tamano_detalles.text

    Rectangle {
        id: rectangle
        color: "#000000"
        anchors.fill: parent
        opacity: 0.75
    }

    ColumnLayout {
        anchors.right: parent.right
        anchors.rightMargin: 48
        anchors.left: parent.left
        anchors.leftMargin: 48
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 46
        anchors.top: parent.top
        anchors.topMargin: 46
        spacing: 12

        Label {
            id: lbl_nombre
            text: qsTr("Nombre")
            color: "white"
        }

        Label {
            id: lbl_nombre_detalles
            text: qsTr("")
            Layout.preferredHeight: 24
            Layout.preferredWidth: 304
            verticalAlignment: Text.AlignVCenter
            leftPadding: 5
            font.pixelSize: 18
            color: "white"
        }

        Label {
            id: lbl_tamano
            text: qsTr("Tamaño")
            color: "white"
        }

        Label {
            id: lbl_tamano_detalles
            color: "#ffffff"
            text: qsTr("")
            Layout.preferredHeight: 24
            Layout.preferredWidth: 304
            font.pixelSize: 18
            verticalAlignment: Text.AlignVCenter
            leftPadding: 5
        }

        Label {
            id: lbl_fec_sub
            text: qsTr("Fecha de subida")
            color: "white"
        }

        Label {
            id: lbl_fec_sub_detalles
            color: "#ffffff"
            text: qsTr("")
            Layout.preferredHeight: 24
            Layout.preferredWidth: 304
            font.pixelSize: 18
            verticalAlignment: Text.AlignVCenter
            leftPadding: 5
        }

        Label {
            id: lbl_fec_mod
            text: qsTr("Fecha de Modificación")
            color: "white"
        }

        Label {
            id: lbl_fec_mod_detalles
            color: "#ffffff"
            text: qsTr("")
            Layout.preferredHeight: 24
            Layout.preferredWidth: 304
            font.pixelSize: 18
            verticalAlignment: Text.AlignVCenter
            leftPadding: 5
        }

        Label {
            id: lbl_duenio
            text: qsTr("Dueño")
            color: "white"
        }

        Label {
            id: lbl_duenio_detalles
            color: "#ffffff"
            text: qsTr("")
            Layout.preferredHeight: 24
            Layout.preferredWidth: 304
            font.pixelSize: 18
            verticalAlignment: Text.AlignVCenter
            leftPadding: 5
        }
    }
}
