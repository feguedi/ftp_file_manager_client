import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.0

Rectangle {
    id: rectangle
    //width: 400
    //height: 400
    property alias btn_login: btn_login
    property alias login: rectangle
    anchors.fill: parent

    ColumnLayout {
        x: 84
        y: 84
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 45

        GridLayout {
            id: grid
            rowSpacing: 30
            rows: 3
            columns: 3

            TextField {
                id: txt_dir
                maximumLength: 50
                Layout.preferredHeight: 40
                Layout.preferredWidth: 159
                placeholderText: qsTr("Dirección")
            }

            Label {
                id: lbl_puntos
                text: qsTr(":")
                font.pointSize: 22
            }

            TextField {
                id: txt_puerto
                validator: IntValidator { bottom: 1; top: 9999 }
                horizontalAlignment: Text.AlignHCenter
                Layout.preferredHeight: 40
                Layout.preferredWidth: 56
                placeholderText: qsTr("Puerto")
                maximumLength: 4
            }

            TextField {
                id: txt_usuario
                maximumLength: 50
                Layout.columnSpan: 3
                Layout.preferredHeight: 40
                Layout.preferredWidth: 232
                placeholderText: qsTr("Usuario")
            }

            TextField {
                id: txt_contra
                maximumLength: 100
                Layout.columnSpan: 3
                Layout.preferredHeight: 40
                Layout.preferredWidth: 232
                echoMode: TextInput.Password
                placeholderText: qsTr("Contraseña")
            }
        }

        Button {
            id: btn_login
            text: qsTr("Entrar")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }
    }
}
