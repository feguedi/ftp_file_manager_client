import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.0

Rectangle {
    id: manager
    anchors.fill: parent
    property alias manager: manager
    property alias usuario: btn_usuario
    property alias menu_usuario: menuUsuario
    property alias salir: salir
    property alias lbl_directorio: lbl_directorio.text
    property alias lbl_url: lbl_url.text

    GridLayout {
        id: encabezado
        height: 54
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.top: parent.top
        anchors.topMargin: 15
        rows: 2
        columns: 2

        Label {
            id: lbl_url
            text: qsTr("URL")
            font.pointSize: 12
        }

        Button {
            id: btn_usuario
            text: "Usuario"
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.rowSpan: 2

            background: Rectangle { color:"transparent" }

            Menu {
                id: menuUsuario
                x: manager.width -width
                transformOrigin: Menu.TopRight

                MenuItem {
                    id: salir
                    text: "Salir"
                }
            }
        }

        Label {
            id: lbl_directorio
            text: qsTr("Directorio")
            font.pointSize: 16
        }
    }

    Pane {
        id: pane
        bottomPadding: 0
        rightPadding: 0
        leftPadding: 0
        topPadding: 0
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.top: encabezado.bottom
        anchors.topMargin: 6

        ColumnLayout {
            id: contenido
            anchors.fill: parent
            spacing: 3
            Repeater {
                anchors.fill: parent
                model: obtenerDirectorio
                delegate: archivoDelegate
            }
        }
    }

    BotonSubirForm {
        id: botonSubir
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

}
