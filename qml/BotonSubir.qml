import QtQuick 2.4
import QtQuick.Dialogs 1.2

BotonSubirForm {
    FileDialog {
        id: fileDialog
            title: "Por favor elija un archivo"
            folder: shortcuts.home
            onAccepted: {
                console.log("Elegiste: " + fileDialog.fileUrls)
                Qt.quit()
            }
            onRejected: {
                console.log("Canceled")
                Qt.quit()
            }
            Component.onCompleted: visible = true
    }

    onClicked: { console.log("Elegir archivo"); fileDialog.visible = true }
}
