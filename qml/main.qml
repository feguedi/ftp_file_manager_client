import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.0

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Cliente FTP")
    Repeater {

    }

    Login {
        id: log_in
        btn_login.onClicked: { archivos }
    }
    Manager {
        id: archivos
        salir.onClicked: { log_in }
    }

    SequentialAnimation {
        id: animacion

    }
}
