import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

ManagerForm {
    property alias tipo: tipo.source
    property alias nombre: nombre.text
    property alias lugarMenu: opcionesMenu.transformOrigin

    usuario.onClicked: { menu_usuario.open() }

    Component {
        id: archivoDelegate

        RowLayout {
            id: archivo
            height: 25
            anchors.fill: parent

            Image {
                id: tipo
                anchors.left: parent
                anchors.leftMargin: 0
            }
            Label {
                id: nombre
                anchors.fill: parent
            }
            Button {
                width: 25
                id: opciones
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

                contentItem: Image {
                    source: "img/more.png"
                    fillMode: Image.PreserveAspectFit
                }

                onClicked: opcionesMenu.open()

                Menu {
                    id: opcionesMenu
                    x: opciones.width - width

                    MenuItem {
                        text: qsTr("Cambiar nombre")
                    }

                    MenuItem {
                        text: qsTr("Descargar")
                    }

                    MenuItem {
                        text: qsTr("Eliminar")
                    }

                    MenuItem {
                        text: qsTr("Detalles")
                    }
                }
            }
        }
    }
}
