import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0

Button {
    id: button
    width: 45
    height: 45

    property alias pressBoton: button

    background: Rectangle {
        color: "transparent"
    }

    contentItem: Image {
        id: plus
        anchors.fill: parent
        source: "img/plus.png"
        fillMode: Image.PreserveAspectFit
    }
}
