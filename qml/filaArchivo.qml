import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

Component {
    property alias tipo: tipo.source
    property alias nombre: nombre.text
    property alias lugarMenu: opcionesMenu.transformOrigin

    RowLayout {
        Image {
            id: tipo
            anchors.left: parent
            anchors.leftMargin: 0
        }
        Label {
            id: nombre
            anchors.fill: parent
        }
        Button {
            height: 25; width: 25
            id: opciones
            contentItem: Image {
                source: "img/more.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: opcionesMenu.open()

            Menu {
                id: opcionesMenu
                x: opciones.width - width

                MenuItem {
                    text: qsTr("Cambiar nombre")
                    onTriggered: {  }
                }

                MenuItem {
                    text: qsTr("Descargar")
                    onTriggered: {  }
                }

                MenuItem {
                    text: qsTr("Eliminar")
                    onTriggered: {  }
                }

                MenuItem {
                    text: qsTr("Detalles")
                    onTriggered: {  }
                }
            }
        }
    }
}
