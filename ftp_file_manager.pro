QT += qml quick quickcontrols2 network core

CONFIG += c++11

SOURCES += main.cpp \
    src/*.cpp

RESOURCES += qml.qrc

# OTHER_FILES += *.qml

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    img/plus.png \
    img/more.png
    README.md

HEADERS += src/*.h
