#ifndef FILEHOLDER_H
#define FILEHOLDER_H

#include <QObject>
#include <QFile>
#include <QHash>
#include <QStringList>
#include <QDebug>
#include <QByteArray>
#include <QBuffer>
#include <QCryptographicHash>
#include <QNetworkInterface>
#include <QDateTime>

#include "config.h"

class FileHolder : public QObject
{
    Q_OBJECT
public:
    explicit FileHolder(QObject *parent = 0);

    static void insertarArchivo()
    {
        QString archivoNombre = "archivo_" + QString::number( obtnArchivoCuenta() );
        FileHolder::archivosLista.append( archivoNombre);
        //FileHolder::archivos.insert( archivoNombre);

    }


    static QString archivos;
    static QStringList archivosLista;

    static int obtnArchivoCuenta(void)
    {
        return FileHolder::archivosLista.count();
    }
};

#endif // FILEHOLDER_H
