#include "uploader.h"
#include <QFile>
#include <QDebug>

#include <config.h>
#include <fileholder.h>

Uploader::Uploader(QObject *parent) : QThread(parent)
{
    this->subidoNombre = "";
    this->subirman = new QNetworkAccessManager(this);
    this->estaUploading = false;
    this->limpiar = false;
}

void Uploader::run()
{
    this->tiempoPosponer = new QTimer(this);
    this->tiempoPosponer->setSingleShot(false);
    connect( this->tiempoPosponer, SIGNAL( timeout() ), this, SLOT( subir() ) );
    this->tiempoPosponer->start(500);

    this->tiempoLimpiar = new QTimer(this);
    this->tiempoLimpiar->setSingleShot(false);
    connect( this->tiempoLimpiar, SIGNAL( timeout() ), this, SLOT( limpiando() ) );
    this->tiempoLimpiar->start(5000);
}

void Uploader::limpiando()
{
    if( FileHolder::obtnArchivoCuenta() > 0 )
    {
        tiempoPosponer->stop();
        subirSig();
    }
}
