#ifndef UPLOADER_H
#define UPLOADER_H

#include <QThread>
#include <QTimer>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QUrl>

class Uploader : public QThread
{
    Q_OBJECT
public:
    explicit Uploader(QObject *parent = 0);

    QString portapapelesNombre;
    void run();
    QNetworkAccessManager *subirman;
    QString subidoNombre;

private:
    void subirSig();
    void conectSenales();
    QUrl cargarURL();

    QTimer *tiempoPosponer;
    QTimer *tiempoLimpiar;
    QNetworkReply *_netRep;
    bool estaUploading;
    bool limpiar;

signals:
    void updateTrayIconProgress( int val );
    void updateClipBoard(QString nuevaUrl );
    void mostrarMensaje();

public slots:
    void subir();
    void limpiando();

    void datosEnviar();
    void datosError(QNetworkReply::NetworkError);
    void SetProgress(qint64,qint64);
};

#endif // UPLOADER_H
