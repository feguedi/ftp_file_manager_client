#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>

class config : public QObject
{
public:
    explicit config(QObject *parent = 0);

    static QString hostName;
    static QString hostUsername;
    static QString hostPassword;
    static QString hostURL;
    static QString hostPort;
};

#endif // CONFIG_H
