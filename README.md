# FTP File Manager

Software para manejo de archivos mediante un servidor FTP utilizando una interfaz QML ([Qt Quick Controls 2](http://qt.io)).

## Íconos

[File Types](https://www.flaticon.com/packs/file-types) designed by [Smashicons](https://www.flaticon.com/authors/smashicons) from Flaticon.